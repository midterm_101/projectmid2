/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.project2mid;

import static com.wutinan.project2mid.JobProgrammer.Name;

/**
 *
 * @author INGK
 */
public class TestProject {

    public static void main(String[] args) {
        Programmer1 joe = new Programmer1("Joe", "C++", 5, 6);
        joe.learn2();
        joe.learn1();
        Name("Joe" + " is Programmer : " + (joe instanceof JobProgrammer));
        System.out.println("----------");
        
        Programmer2 jane = new Programmer2("Jane", "Java", 10, 5);
        jane.learn2();
        jane.learn1();
        Name("Jane" + " is Programmer : " + (jane instanceof JobProgrammer));
        System.out.println("----------");
        
        Programmer3 Nat = new Programmer3("Nat", "Python, Java ", 1, 4);
        jane.learn2();
        jane.learn1();
        Name("Nat" + " is Programmer : "+ (Nat instanceof JobProgrammer));
        System.out.println("----------");
        
    }

    
}
